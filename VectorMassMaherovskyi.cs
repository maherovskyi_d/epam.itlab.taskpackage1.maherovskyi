using System;
using System.Text;

namespace Epam.TaskPackage1.VectorMass.Maherovskyi
{
    /// <summary>
    /// One dimentional vector with flowing indexer entry point & methods to work with it.
    /// </summary>
    class VectorMass
    {
        /// <summary>
        /// Contains all the elements of Vector.
        /// </summary>
        Array _array;
        int _startingIndexer;

        /// <summary>
        /// Class constructor. Indexer setable.
        /// <param name="startingIndexer"> Indexer, from which the count of elements begins. </param>
        /// <param name="vector"> One-dimentional array of integers. </param> 
        /// </summary>
        public VectorMass(int startingIndexer, int[] vector)
        {
            _startingIndexer = startingIndexer;
            _array = Array.CreateInstance(typeof(int), new int[] { vector.Length }, new int[] { _startingIndexer });
            for (var i = 0; i < Length; i++)
            {
                _array.SetValue(vector[i], i + _startingIndexer);
            }
        }

        /// <summary>
        /// Class constructor. Indexer set default by 0.
        /// </summary>
        /// <param name="vector"></param>
        public VectorMass(params int[] vector)
        {
            _startingIndexer = 0;
            _array = Array.CreateInstance(typeof(int), new int[] { vector.Length }, new int[] { _startingIndexer });
            for (var i = 0; i < Length; i++)
            {
                _array.SetValue(vector[i], i + _startingIndexer);
            }
        }

        /// <summary>
        /// Returns the number of elements in the Vector.
        /// </summary>
        public int Length
        {
            get
            {
                return _array.Length;
            }
        }

        public int StartingIndexer
        {
            get
            {
                return _startingIndexer;
            }
        }

        public int this[int i]
        {
            get
            {
                if (i < StartingIndexer || i > StartingIndexer + Length)
                {
                    throw new IndexOutOfRangeException("No such index into this array.");
                }
                return (int)_array.GetValue(i);
            }
            set
            {
                if (i < StartingIndexer || i > StartingIndexer + Length)
                {
                    throw new IndexOutOfRangeException("No such index into this array.");
                }
                _array.SetValue(value, i);
            }
        }

        /// <summary>
        /// Returns vector in the {{0}, {1}, ...} form.
        /// </summary>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder(32);
            sb.Append("{ ");
            for (var i = _startingIndexer; i < _array.Length + _startingIndexer - 1; i++)
            {
                sb.AppendFormat("{0}, ", _array.GetValue(i));
            }
            sb.AppendFormat("{0} ", _array.GetValue(_array.Length + _startingIndexer - 1).ToString());
            sb.Append("}");
            return sb.ToString();
        }

        /// <summary>
        /// Returns vector HashCode.
        /// </summary>
        public override int GetHashCode()
        {
            return _array.GetHashCode();
        }

        /// <summary>
        /// Comparison by hash code.
        /// </summary>
        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is VectorMass))
            {
                return false;
            }

            return GetHashCode() == obj.GetHashCode();
        }

        /// <summary>
        /// Overloads Add(a, b) method.
        /// </summary>
        public static VectorMass operator +(VectorMass a, VectorMass b)
        {
            return Add(a, b);
        }

        /// <summary>
        /// Overloads Sub(a, b) method.
        /// </summary>
        public static VectorMass operator -(VectorMass a, VectorMass b)
        {
            return Sub(a, b);
        }

        /// <summary>
        /// Overloaded ScallarMul() method.
        /// </summary>
        public static VectorMass operator *(int scallar, VectorMass a)
        {
            return ScalarMul(scallar, a);
        }

        public static VectorMass operator *(VectorMass a, int scallar)
        {
            return ScalarMul(scallar, a);
        }

        /// <summary>
        /// Comparison operators overloads.
        /// </summary>
        public static bool operator ==(VectorMass a, VectorMass b)
        {
            return Equals(a, b);
        }

        public static bool operator !=(VectorMass a, VectorMass b)
        {
            return !Equals(a, b);
        }

        /// <summary>
        /// Compares 2 vectors value by value.
        /// </summary>
        static bool Equals(VectorMass a, VectorMass b)
        {
            if (a.Length != b.Length)
            {
                return false;
            }
            else
            {
                bool equals = true;
                for (var i = 0; i < a.Length; i++)
                {
                    if (a[i + a.StartingIndexer] != b[i + b.StartingIndexer])
                    {
                        equals = false;
                        break;
                    }
                }
                return equals;
            }
        }

        /// <summary>
        /// Adds 2 vectors element by element if their Length is same.
        /// </summary>
        /// <returns> Vector with Starting index of VectorMass "a". </returns>
        static VectorMass Add(VectorMass a, VectorMass b)
        {
            if (a.Length != b.Length)
            {
                throw new Exception("Cannot add 2 vectors with different Length");
            }

            for (var i = 0; i < a.Length; i++)
            {
                a[i + a.StartingIndexer] += b[i + b.StartingIndexer];
            }

            return a;
        }

        /// <summary>
        /// Substructs 2 vectors element by element if their Length is same.
        /// </summary>
        /// <returns> Vector with Starting index of VectorMass "a". </returns>
        static VectorMass Sub(VectorMass a, VectorMass b)
        {
            if (a.Length != b.Length)
            {
                throw new Exception("Cannot substract 2 vectors with different Length");
            }

            for (var i = 0; i < a.Length; i++)
            {
                a[i + a.StartingIndexer] -= b[i + b.StartingIndexer];

            }

            return a;
        }

        /// <summary>
        /// Multiplies each Vector element by scalar.
        /// </summary>
        static VectorMass ScalarMul(int scalar, VectorMass a)
        {
            for (var i = 0; i < a.Length; i++)
            {
                a[i + a.StartingIndexer] *= scalar;
            }
            return a;
        }
    }

    class Program
    {
        static void Main(string[] atgs)
        {
            VectorMass v3 = new VectorMass(4, new int[] { 1, 2, 3, 4, 5 });
            Console.WriteLine(v3);
            Console.WriteLine("Vector starting indexer is: {0}", v3.StartingIndexer);
            VectorMass v4 = new VectorMass(1, 2, 3, 4, 5);
            Console.WriteLine(v4);
            Console.WriteLine("Vector starting indexer is: {0}", v4.StartingIndexer);
            v4 *= 3;
            Console.WriteLine(v4);
            v3[4] = 4;
            Console.WriteLine(v3[4]);
            Console.WriteLine((v3 + v4).ToString());
            Console.WriteLine((v3 - v4).ToString());
        }
    }
}